FROM ubuntu:18.04

RUN env | sort; apt-get update; \
apt-get install -y iproute2 telnet swaks;\
apt-get clean

ENTRYPOINT "/bin/bash"

